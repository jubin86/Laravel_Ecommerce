@extends('welcome')
@section('content')

    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Check out</li>
                </ol>
            </div><!--/breadcrums-->



            <div class="register-req">
                <p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
            </div><!--/register-req-->

            <div class="shopper-informations">
                <div class="row">
                    {{--<div class="col-sm-3">--}}
                        {{--<div class="shopper-info">--}}
                            {{--<p>Shopper Information</p>--}}
                            {{--<form>--}}
                                {{--<input type="text" placeholder="Display Name">--}}
                                {{--<input type="text" placeholder="User Name">--}}
                                {{--<input type="password" placeholder="Password">--}}
                                {{--<input type="password" placeholder="Confirm password">--}}
                            {{--</form>--}}
                            {{--<a class="btn btn-primary" href="">Get Quotes</a>--}}
                            {{--<a class="btn btn-primary" href="">Continue</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="col-sm-12 clearfix">
                        <div class="bill-to">
                            <p>Bill To</p>
                            <div class="form-one">

                                    {!! Form::open(array('url'=>'/shipping_store')) !!}

                                    <input type="email" placeholder="Email*" name="shipping_mail">
                                    <input type="text" placeholder="First Name *" name="firstname">
                                    <input type="text" placeholder="Last Name *" name="lastname">
                                    <input type="text" placeholder="Address *" name="address">
                                    <input type="text" placeholder="city *" name="city">
                                    <input type="text" placeholder="Mobile Phone" name="mobile_number">
                                    <button class="btn btn-primary">Send</button>


                                {!! Form::close() !!}
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <div class="review-payment">
                <h2>Review & Payment</h2>
            </div>


            <div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
                <span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
                <span>
						<label><input type="checkbox"> Paypal</label>
					</span>
            </div>
        </div>
    </section> <!--/#cart_items-->


    @endsection