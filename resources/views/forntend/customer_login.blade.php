@extends('welcome')
@section('content')

    <section id="form"><!--form-->
        <p>
            <?php
            $session=Session::get('message');
            if($session){
                echo $session;
                Session::put('message',null);
            }
            ?>
        </p>
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="login-form"><!--login form-->
                        <h2>Login to your account</h2>
                        <form action="{{url('/login_check')}}" method="post">
                            {{csrf_field()}}

                            <input type="email" placeholder="Email Address" name="email" />
                            <input type="text" placeholder="fullname" name="password" />

                            <button type="submit" class="btn btn-default">Login</button>
                        </form>
                    </div><!--/login form-->
                </div>
                <div class="col-sm-1">
                    <h2 class="or">OR</h2>
                </div>
                <div class="col-sm-4">
                    <div class="signup-form"><!--sign up form-->
                        <h2>New User Signup!</h2>
                        <form action="{{url('/customer_signup')}}" method="post">
                            {{csrf_field()}}
                            <input type="text" placeholder="Fullname" name="fullname" required/>
                            <input type="email" placeholder="xyz@gmail.com"  name="email" required/>
                            <input type="text" placeholder="password"  name="password" required/>
                            <input type="text" placeholder="mobile number"  name="mobile_number" required/>
                            <button type="submit" class="btn btn-default">Signup</button>
                        </form>
                    </div><!--/sign up form-->
                </div>
            </div>
        </div>
    </section><!--/form-->
    @endsection