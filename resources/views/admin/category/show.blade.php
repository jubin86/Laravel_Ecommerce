
@extends('admin.layouts.master')
@section('main-title','category-show-page')

@section('content')
    <h2>category show</h2>
    <p class="alert-success">
        <?php
        $session=Session::get('message');
        if ($session){
            echo $session;
            Session::put('message',null);
        }
        ?>

    </p>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>status</th>
            <th>action</th>

        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
        <tr>
            <td>{{$category->title}}</td>
            <td>{{$category->description}}</td>
            <td>
                @if($category->publication_status==1)
                <span class="label label-success">{{--{{$category->publication_status}}--}}active</span>

               @else
                    <span class="label label-success">{{--{{$category->publication_status}}--}}unactive</span>
                    @endif
            </td>

            <td>
                @if($category->publication_status==1)
             <a class="btn btn-success" href="{{URL::to('/admin/unactive-category/'.$category->id)}}">
                <i class="halflings-icon white thumbs-down ">stop</i>
             </a>
            @else
                    <a class="btn btn-success" href="{{URL::to('/admin/active-category/'.$category->id)}}">
                        <i class="halflings-icon white thumbs-down ">run</i>
                    </a>
                    @endif
                <a class="btn btn-info" href="{{URL::to('/admin/edit-category/'.$category->id)}}">
                    <i class="fa fa-edit">edit</i>
                </a>
                <a class="btn btn-info"href="{{URL::to('/admin/delete-category/'.$category->id)}}">
                    <i class="fa fa-delete">delete</i>
                </a>
            </td>

        </tr>
           @endforeach

        </tbody>
    </table>
    @endsection