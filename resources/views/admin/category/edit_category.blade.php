@extends('admin.layouts.master')
@section('main-title','edit category page')
@section('content')

    <div class="panel panel-default" style="margin:0px auto; width:450px; margin-top:100px">
        {{--{!! Form::open(array('url' => 'admin/category/store')) !!}--}}
        {{--<form action="{{url('/admin/update-category/'.$category_info->id)}}" method="post">--}}
        {!! Form::open(['route'=>['admin.update-category.store',$category_info->id]]) !!}

        <div class="form-group">


            <label for="exampleInputEmail1">Category Title</label>

            {{--{!!Form::text('title',null,['class'=>'form-control'],['placeholder'=>'Title']);!!}--}}
            <input type="text" name="title", class="form-control" value="{{$category_info->title}}">

            <small id="emailHelp" class="form-text text-muted"></small>
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Category Description</label>

{{--            {!! Form::text('description',null,['class'=>'form-control'],['placeholder'=>'description'],['id'=>'exampleInputPassword1']) !!}--}}
            <input type="text" name="description", class="form-control" value="{{$category_info->description}}">
        </div>

        {{--<div>
            <label for="exampleInputPassword1">publication status</label>
            <input type="checkbox" name="publication_status" value="1">
        </div>--}}

        {{--<div class="form-check">
            {!! Form::submit('save',['class'=>'btn btn-primary']) !!}

        </div>--}}
           <button type="submit" class="btn btn-primary">save</button>
        {{--</form>--}}

        {!! Form::close() !!}
    </div>










@endsection