@extends('admin.layouts.master')
@section('main-title','category-show-page')

@section('content')
    <h2>Order show</h2>
    <p class="alert-success">
<!--        --><?php
//        $session=Session::get('message');
//        if ($session){
//            echo $session;
//            Session::put('message',null);
//        }
//        ?>

    </p>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Order Id</th>
            <th>Customer Id</th>
            <th>Order Total</th>
            {{--<th>price</th>--}}
            {{--<th>category_name</th>--}}
            {{--<th>manufacture name</th>--}}
            <th>Customer Name</th>
            <th>Status</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>
        {{--@foreach($items as $item)--}}
        {{--<tr>--}}
        {{--<td>{{$item->title}}</td>--}}
        {{--<td>{{$item->description}}</td>--}}
        {{--</tr>--}}
        {{--@endforeach--}}

        @foreach($manage_product_order as $order)
            <tr>

                <td>{{$order->order_id}}</td>
                <td>{{$order->customer_id}}</td>
                {{--<td><img src="{{URL::to($order->product_image)}}" style="height: 80px;width: 80px" alt=""></td>--}}
                <td>{{$order->order_total}}</td>
                <td>{{$order->fullname}}</td>

                <td>
                    @if($order->order_status==1)
                    <span class="label label-success">Done</span>
                    @else
                        <span class="label label-success">pending</span>
                    @endif

                </td>

                <td>


                        <a class="btn btn-success" href="{{URL::to('/admin/pause-product/')}}">
                            stop
                        </a>

                        <a class="btn btn-success" href="{{URL::to('/admin/active-product/')}}">
                            run
                        </a>


                    <a class="btn btn-success" href="{{URL::to('admin/manage_order_show/'.$order->order_id)}}">edit</a>
                    <a class="btn btn-danger" href="{{URL::to('admin/product_delete/')}}">delete</a>


                </td>



            </tr>
        @endforeach




        </tbody>
    </table>
@endsection