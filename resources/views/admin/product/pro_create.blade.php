@extends('admin.layouts.master')
@section('main-title','category new add')
@section('content')
<p class="alert-success">
    <?php
    $message=Session::get('message');
    if($message){
        echo $message;
        Session::put('message',null);
    }
    ?>


</p>

    <div class="panel panel-default" style="margin:0px auto; width:450px; margin-top:100px">
        {!! Form::open(array('url' => 'admin/product_store','enctype'=>'multipart/form-data')) !!}

      {{--  <form action="{{url('admin/product_store')}}" method="post" enctype="multipart/form-data">--}}
        <div class="form-group">


            <label for="exampleInputEmail1">Product name</label>

            {!!Form::text('product_name',null,['class'=>'form-control'],['placeholder'=>'Title']);!!}

            <small id="emailHelp" class="form-text text-muted"></small>
        </div>

        <div class="control-group">
            <label class="control-label" for="controls">Product Category</label>
            <div class="controls">

                <select id="selectError"name="category_id">
                    <option>Select category</option>
                    <?php
                    $categories= DB::table('categories')
                        ->where('publication_status',1)
                        ->get();
                    foreach($categories as $category){
                    ?>

                    <option value="{{$category->id}}">{{$category->title}}</option>
                      <?php  }?>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="controls">Product Manufacture</label>
            <div class="controls">

                <select id="selectError"name="manufacture_id">
                    <option>Select manufacture</option>
                    <?php
                    $manufacture= DB::table('manufacture')
                        ->where('publication_status',1)
                        ->get();
                    foreach($manufacture as $manufac){
                    ?>

                    <option value="{{$manufac->manufacture_id}}">{{$manufac->manufacture_title}}</option>
                    <?php  }?>
                </select>
            </div>
        </div>



        <div class="form-group">
            <label for="exampleInputPassword1">product shortdescribe</label>

            {!! Form::text('pro_shortdetails',null,['class'=>'form-control'],['placeholder'=>'description'],['id'=>'exampleInputPassword1']) !!}
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">product longDescribe</label>

            {!! Form::text('pro_longdetails',null,['class'=>'form-control'],['placeholder'=>'description'],['id'=>'exampleInputPassword1']) !!}
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Product price</label>

            {!! Form::text('product_price',null,['class'=>'form-control'],['placeholder'=>'description'],['id'=>'exampleInputPassword1']) !!}
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">product size</label>

            {!! Form::text('product_size',null,['class'=>'form-control'],['placeholder'=>'description'],['id'=>'exampleInputPassword1']) !!}
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">product color</label>

            {!! Form::text('product_color',null,['class'=>'form-control'],['placeholder'=>'description'],['id'=>'exampleInputPassword1']) !!}
        </div>

        <div class="control-group">
            <label class="control-label" for="controls">Product image</label>
            <div class="controls">
                <input class="input-file uniform_on" id="fileInput" type="file" name="product_image">
            </div>
        </div>



        <div>
            <label for="exampleInputPassword1">publication status</label>
            <input type="checkbox" name="publication_status" value="1">
        </div>



        <div class="form-check">
            {!! Form::submit('save',['class'=>'btn btn-primary']) !!}
            {{--<button type="submit" class="btn btn-primary">add product</button>--}}

        </div>

       {{-- </form>--}}

        {!! Form::close() !!}
    </div>










@endsection