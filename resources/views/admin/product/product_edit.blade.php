@extends('admin.layouts.master')
@section('main-title','category new add')
@section('content')
    <p class="alert-success">
        <?php
        $message=Session::get('message');
        if($message){
            echo $message;
            Session::put('message',null);
        }
        ?>


    </p>

    <div class="panel panel-default" style="margin:0px auto; width:450px; margin-top:100px">
        {!! Form::open(['route'=>['admin.update_product.store',$product_info->product_id],'enctype'=>'multipart/form-data']) !!}
        {{--<form action="{{url('admin/update_product',$product_info->product_id)}}" method="post" enctype="multipart/form-data">--}}


        <div class="form-group">
            <label for="exampleInputEmail1">Product name</label>
            <input type="text" name="product_name" class="form-control" value="{{$product_info->product_name}}">
        </div>

        <div class="control-group">
            <label class="control-label" for="controls">Product Category</label>

            <div class="controls">
                <select id="selectError"name="category_id">
                    <option>Select category</option>
                    <?php
                    $categories= DB::table('categories')
                        ->where('publication_status',1)
                        ->get();
                    foreach($categories as $category){
                    ?>

                    <option value="{{$category->id}}">{{$category->title}}</option>
                    <?php  }?>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="controls">Product Manufacture</label>
            <div class="controls">

                <select id="selectError"name="manufacture_id">
                    <option>Select manufacture</option>
                    <?php
                    $manufacture= DB::table('manufacture')
                        ->where('publication_status',1)
                        ->get();
                    foreach($manufacture as $manufac){
                    ?>
                    <option value="{{$manufac->manufacture_id}}">{{$manufac->manufacture_title}}</option>
                    <?php  }?>
                </select>
            </div>
        </div>



        <div class="form-group">

            <label for="exampleInputPassword1">product shortdescribe</label>
            <input type="text" class="form-control"name="pro_shortdetails"value="{{$product_info->pro_shortdetails}}">
        </div>

        <div class="form-group">
        <label for="exampleInputPassword1">product longDescribe</label>


        <input name="pro_longdetails" id="" cols="30" rows="10" value="{{$product_info->pro_longdetails}}">
        </div>


        <div class="form-group">
            <label for="exampleInputPassword1">Product price</label>
            <input type="number" class="form-control"name="product_price"value="{{$product_info->product_price}}">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">product size</label>
            <input type="text" class="form-control"name="product_size"value="{{$product_info->product_size}}">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">product color</label>
            <input type="text" class="form-control"name="product_color"value="{{$product_info->product_color}}">
        </div>

        <div class="control-group">
            <label class="control-label" for="controls">Product image</label>
            <div class="controls">
                <input class="input-file uniform_on" id="fileInput" type="file" name="product_image" value="{{$product_info->product_image}}">
            </div>
        </div>


              <div class="form-check">
            {{--{!! Form::submit('save',['class'=>'btn btn-primary']) !!}--}}
            <button type="submit" class="btn btn-primary">update product</button>

        </div>

         {{--</form>--}}

        {!! Form::close() !!}
    </div>










@endsection