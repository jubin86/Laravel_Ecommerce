@extends('admin.layouts.master')
@section('main-title','category-show-page')

@section('content')
    <h2>Product show</h2>
    <p class="alert-success">
        <?php
        $session=Session::get('message');
        if ($session){
            echo $session;
            Session::put('message',null);
        }
        ?>

    </p>
    <table class="table table-hover">
        <thead>
        <tr> <th>Product_id</th>
            <th>name</th>
            <th>image</th>
            <th>price</th>
            <th>category_name</th>
            <th>manufacture name</th>
            <th>Status</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>
        {{--@foreach($items as $item)--}}
        {{--<tr>--}}
        {{--<td>{{$item->title}}</td>--}}
        {{--<td>{{$item->description}}</td>--}}
        {{--</tr>--}}
        {{--@endforeach--}}

        @foreach($products as $item)
            <tr>

                <td>{{$item->product_id}}</td>
                <td>{{$item->product_name}}</td>
                <td><img src="{{URL::to($item->product_image)}}" style="height: 80px;width: 80px" alt=""></td>
                <td>{{$item->product_price}}</td>
                <td>{{$item->title}}</td>
                <td>{{$item->manufacture_title}}</td>



                <td>
                    @if($item->publication_status==1)

                        <span class="label label-success">active</span>

                    @else

                        <span class="label label-success">unactive</span>

                    @endif



                </td>

                <td>

                    @if($item->publication_status==1)
                        <a class="btn btn-success" href="{{URL::to('/admin/pause-product/'.$item->product_id)}}">
                           stop
                        </a>
                    @else
                        <a class="btn btn-success" href="{{URL::to('/admin/active-product/'.$item->product_id)}}">
                          run
                        </a>
                    @endif

                    <a class="btn btn-success" href="{{URL::to('admin/product_edit/'.$item->product_id)}}">edit</a>
                    <a class="btn btn-danger" href="{{URL::to('admin/product_delete/'.$item->product_id)}}">delete</a>


                </td>



            </tr>
        @endforeach




        </tbody>
    </table>
@endsection