@extends('admin.layouts.master');
@section('content')
    <h2>List of users</h2>

    <table class="table">
        <thead>
        <tr>

            <th>name</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>

        </tr>
        @endforeach
        </tbody>
    </table>
    @endsection