@extends('admin.layouts.master')
@section('main-title','category-show-page')

@section('content')
    <h2>Item show</h2>
    <table class="table table-hover">
        <thead>
        <tr> <th>Manufacture_id</th>
            <th>Title</th>
            <th>Description</th>
            <th>Status</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>
        {{--@foreach($items as $item)--}}
            {{--<tr>--}}
                {{--<td>{{$item->title}}</td>--}}
                {{--<td>{{$item->description}}</td>--}}
            {{--</tr>--}}
        {{--@endforeach--}}

        @foreach($manufactures as $item)
            <tr>

                <td>{{$item->manufacture_id}}</td>
                <td>{{$item->manufacture_title}}</td>
                <td>{{$item->manufacture_details}}</td>



                <td>
                    @if($item->publication_status==1)
                        <span class="btn btn-success">active</span>
                        @else
                        <span class="btn btn-success">unactive</span>
                        @endif
                </td>
                <td>
                    @if($item->publication_status==1)
                    <a class="btn btn-success" href="{{URL::to('/admin/manufacture_pause/'.$item->manufacture_id)}}">pause</a>
                        @else

                        <a class="btn btn-success" href="{{URL::to('admin/manufacture_run/'.$item->manufacture_id)}}">run</a>
                        @endif

                        <a class="btn btn-success" href="{{URL::to('admin/manufacture_edit/'.$item->manufacture_id)}}">edit</a>
                        <a class="btn btn-danger" href="{{URL::to('admin/manufacture_delete/'.$item->manufacture_id)}}">delete</a>


                </td>



            </tr>
            @endforeach




        </tbody>
    </table>
@endsection