@extends('admin.layouts.master')
@section('main-title','item_add')
@section('content')
    <div class="panel panel-default" style="margin:0px auto; width:450px; margin-top:100px">
        {!! Form::open(array('url' => 'admin/manufacture/store')) !!}

        <div class="form-group">


            <label for="exampleInputEmail1">manufacture Title</label>

            {!!Form::text('manufacture_title',null,['class'=>'form-control'],['placeholder'=>'Title']);!!}

            <small id="emailHelp" class="form-text text-muted"></small>
        </div>






        <div class="form-group">
            <label for="exampleInputPassword1">manufacture Description</label>

            {!! Form::text('manufacture_details',null,['class'=>'form-control'],['placeholder'=>'description'],['id'=>'exampleInputPassword1']) !!}
        </div>


        <div>
            <label for="exampleInputPassword1">publication status</label>
           <input type="checkbox" name="publication_status" value="1">
        </div>

        <div class="form-check">
            {!! Form::submit('save',['class'=>'btn btn-primary']) !!}

        </div>


        {!! Form::close() !!}
    </div>

@endsection