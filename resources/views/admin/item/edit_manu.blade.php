@extends('admin.layouts.master')
@section('main-title','manu_update')
@section('content')
    <div class="panel panel-default" style="margin:0px auto; width:450px; margin-top:100px">

        {!! Form::open(['route'=>['admin.update_manu.store' , $manu_info->manufacture_id]]) !!}






        <div class="form-group">


            <label for="exampleInputEmail1">manufacture Title</label>
            <input type="text" class="form-control" name="manufacture_title" value="{{$manu_info->manufacture_title}}">

            {{--{!!Form::text('manufacture_title',null,['class'=>'form-control'],['placeholder'=>'Title'],['value'=>'accesor']);!!}--}}

            <small id="emailHelp" class="form-text text-muted"></small>
        </div>






        <div class="form-group">
            <label for="exampleInputPassword1">manufacture Description</label><br>

            {{--{!! Form::text('manufacture_details',null,['class'=>'form-control'],['placeholder'=>'description'],['id'=>'exampleInputPassword1']) !!}--}}
            <textarea  name="manufacture_details" class="cleditor" rows="3" cols="30">
                      {{$manu_info->manufacture_details}}</textarea>
        </div>






        <div class="form-check">
            {!! Form::submit('update',['class'=>'btn btn-primary']) !!}

        </div>


        {!! Form::close() !!}
    </div>

@endsection