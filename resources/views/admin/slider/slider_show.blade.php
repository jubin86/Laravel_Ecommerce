
@extends('admin.layouts.master')
@section('main-title','category-show-page')

@section('content')
    <h2>Slider show</h2>
    <p class="alert-success">
        <?php
        $session=Session::get('message');
        if ($session){
            echo $session;
            Session::put('message',null);
        }
        ?>

    </p>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Slider id</th>
            <th>Slider Image</th>
            <th>status</th>
            <th>action</th>

        </tr>
        </thead>
        <tbody>
        @foreach($sliders as $show_slider)
            <tr>
                <td>{{$show_slider->slider_id}}</td>
                <td><img src="{{URL::to($show_slider->slider_image)}}" alt="" style="height: 100px" width="150px"></td>
                <td>
                    @if($show_slider->publication_status==1)
                        <span class="label label-success">{{--{{$category->publication_status}}--}}active</span>

                    @else
                        <span class="label label-success">{{--{{$category->publication_status}}--}}unactive</span>
                    @endif
                </td>

                <td>
                    @if($show_slider->publication_status==1)
                        <a class="btn btn-success" href="{{URL::to('/admin/slider-unactive/'.$show_slider->slider_id)}}">
                            stop
                        </a>
                    @else
                        <a class="btn btn-success" href="{{URL::to('/admin/slider-active/'.$show_slider->slider_id)}}">
                            run
                        </a>
                    @endif

                    <a class="btn btn-info"href="{{URL::to('/admin/slider-delete/'.$show_slider->slider_id)}}">
                        <i class="fa fa-delete">delete</i>
                    </a>
                </td>

            </tr>
        @endforeach

        </tbody>
    </table>
@endsection