<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use DB;
use Cart;

class CartController extends Controller
{
    public function add_cart(Request $request){

        $quantity=$request->quantity;
        $product_id=$request->product_id;
        $product_info=DB::table('products')
            ->where('product_id',$product_id)
            ->first();

        $data['qty']=$quantity;
        $data['id']=$product_info->product_id;
        $data['name']=$product_info->product_name;
        $data['price']=$product_info->product_price;
        $data['options']['image']=$product_info->product_image;
        Cart::add($data);
          return Redirect::to('/add_cart_store');



    }

    public function add_cart_value_store(){

        $all_categories=DB::table('categories')
            ->where('publication_status',1)
            ->get();

        $data=[
            'categories'=>$all_categories,
        ];
        return view('forntend.add_cart');
    }
    public function delete_cart($rowid){
        Cart::update($rowid,0);
        return Redirect::to('/add_cart_store');
    }
    public function update_cart(Request $request){

        $qty=$request->qty;
        $rowId=$request->rowId;

        Cart::update($rowId,$qty);
        return Redirect::to('/add_cart_store');
    }
}
