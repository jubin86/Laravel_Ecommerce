<?php

namespace App\Http\Controllers\User;

use App\Shipping;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use DB;
use Cart;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
session_start();

class CheckoutController extends Controller
{
    public function customer_login()
    {

        return view('forntend.customer_login');
    }

    public function store(Request $request)
    {
        $data = array();
        $data['fullname'] = $request->fullname;
        $data['email'] = $request->email;
        $data['password'] = md5($request->password);
        $data['mobile_number'] = $request->mobile_number;

        $customer_id = DB::table('signup')
            ->insertGetId($data);


        Session::put('id', $customer_id);
        Session::put('fullname', $request->fullname);
        return Redirect::to('/customer_shipping');


    }

    public function customer_shipping()
    {
        return view('forntend.checkout');
    }

    public function shipping_save(Request $request)
    {

        $data = array();
        $data['shipping_mail'] = $request->shipping_mail;
        $data['firstname'] = $request->firstname;
        $data['lastname'] = $request->lastname;
        $data['address'] = $request->address;
        $data['city'] = $request->city;
        $data['mobile_number'] = $request->mobile_number;


        $shipping_table = DB::table('shipping')
            ->insertGetid($data);

        Session::put('shipping_id', $shipping_table);


        return Redirect::to('/payment_option');


    }

    public function payment_option()
    {

        return view('forntend.payment');
    }

    public function login_check(Request $request)
    {

        $email = $request->email;
        $password = md5($request->password);

        $table = DB::table('signup')
            ->where('email', $email)
            ->where('password', $password)
            ->first();


        if ($table) {
            $session = Session::put('id', $table->id);

            return Redirect::to('/customer_shipping');
        } else {
            return Redirect::to('/customer_login');
        }


    }

    public function customer_logout()
    {

        Session::flush();
        return Redirect::to('/');
    }


    public function payment_gateway(Request $request)
    {
        $payment_gateway = $request->payment_method;
        $pdata=array();
        $pdata['payment_method']=$payment_gateway;
        $pdata['payment_status']='0';

        $payment_id=DB::table('payments')
            ->insertGetId($pdata);


        $customer_id=Session::get('id');
        $shipping_id=Session::get('shipping_id');

        $order_data=array();
        $order_data['customer_id']=Session::get('id');
        $order_data['shipping_id']=Session::get('shipping_id');
        $order_data['payment_id']=$payment_id;
        $order_data['order_total']=Cart::total();
        $order_data['order_status']='1';

        $order_id=DB::table('orders')
            ->insertGetId($order_data);

         $contents=Cart::content();


        $details_data=array();

         foreach ($contents as $content)
         {
         $details_data['order_id']=$order_id;
         $details_data['product_id']=$content->id;
         $details_data['product_name']=$content->name;
         $details_data['product_price']=$content->price;
         $details_data['product_sales_quantity']=$content->qty;

         DB::table('order-details')
             ->insert($details_data);
         }

          if ($payment_gateway=='handcash'){
             Cart::destroy();
              return view('forntend.handcash');

          }
          elseif ($payment_gateway=='bkash'){
              echo "succesfully get bkash ";
          }
          elseif ($payment_gateway=='roket'){
              echo "succesfully get roket ";
          }
          elseif ($payment_gateway=='payoneer'){
              echo "succesfully get payoneer ";
          }
          else{
              echo "nothig";
          }







    }
}