<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ProcatshowController extends Controller
{
    public function index($id){

        $product_by_category=DB::table('products')
           ->join('categories','products.category_id','=','categories.id')
           ->select('products.*','categories.title')
           ->where('categories.id',$id)
//           ->where('products.publication_status',1)
           ->limit(18)
           ->get();

//       $catch_pro_cat=view('forntend.product_by_category')
//           ->with('product_by_category',$product_by_category);
//
//       return view('welcome')
//           ->with('forntend.product_by_category',$catch_pro_cat);
        $data=[
            'product_by_category'=>$product_by_category,
        ];

        return view('forntend.product_by_category',$data);
    }
    public function pro_by_menu($manu_id){

       $product_by_manufacture=DB::table('products')
           ->join('manufacture','products.manufacture_id','=','manufacture.manufacture_id')
           ->select('products.*','manufacture.manufacture_title')
           ->where('manufacture.manufacture_id',$manu_id)
           ->limit(18)
           ->get();

        $data=[
            'product_by_manufacture'=>$product_by_manufacture,
        ];
       return view('forntend.product_by_manufac',$data);
    }

    public function product_view($product_id){

        $product_view=DB::table('products')
            ->join('categories','products.category_id','=','categories.id')
            ->join('manufacture','products.manufacture_id','=','manufacture.manufacture_id')
            ->where('products.product_id',$product_id)
            ->where('products.publication_status',1)
            ->first();
         $data=[
             'product_view'=>$product_view,
         ];
         return view('forntend.product_view',$data);


    }


}
