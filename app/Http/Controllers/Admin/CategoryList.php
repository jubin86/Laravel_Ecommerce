<?php

namespace App\Http\Controllers\Admin;
use App\Category;
use Illuminate\Http\Request;
use DB;
use app\Http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\cms;

class CategoryList extends Controller
{
    public function show(){
        $categories=Category::all();


        $data=[
            'categories'=>$categories,
        ];

        return view('admin.category.show',$data);
    }

    public function unactive($id){
       DB::table('categories')
           ->where('id',$id)
           ->update(['publication_status'=>0]);
       Session::put('message',' this category succesfully is pause');
         return Redirect::to ('admin/category/list');




    }

    public function active($id){

        DB::table('categories')->where('id',$id)->update(['publication_status'=>1]);
        Session::put('message',' category succesfully run');
        return Redirect::to('admin/category/list');
    }

    public function edit($id){


       $category_info= DB::table('categories')
            ->where('id',$id)
            ->first();

        $category_info=view('admin.category.edit_category')->with('category_info',$category_info);
        return view('admin.layouts.master')->with('admin.category.edit_category',$category_info);

    }

    public function update(Request $request, $id){

       /*$data=array();
       $data['title']=$request->title;
       $data['description']=$request->description;
        return view('admin.category.testing',$data);*/

     $data=array();
     $data['title']=$request->title;
     $data['description']=$request->description;
     DB::table('categories')
         ->where('id',$id)
         ->update($data);
     return Redirect::to('/admin/category/list');

    }

    public function delete($id){

        DB::table('categories')
            ->where('id',$id)->delete();

        return Redirect::to('admin/category/list');

    }

}
