<?php

namespace App\Http\Controllers\Admin;

use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use session;
use App\Http\Controllers\Admin\cms;



class ManufactureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.item.manufacture_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=($request->all());
        Item::create($data);
        return redirect()->back();
//        $data=array();
//        $data['manufacture_id']=$request->manufacture_id;
//        $data['manufacture_title']=$request->manufacture_title;
//        $data['manufacture_details']=$request->manufacture_details;
//        $data['publication_status']=$request->publication_status;
//        DB::table('manufacture')->insert($data);
//        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
      $items=Item::all();
      $data=[
        'manufactures'=>$items,
      ];
      return view('admin.item.list',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function unactive($manufacture_id){
//        DB::table('manufacture')
//            ->where('manufacture_id',$manufacture_id)
//            ->updtae(['publication_status'=>0]);
//        return Redirect::to('/admin/manufacture/list');
    public function unactive($menu_id){
        DB::table('manufacture')
            ->where('manufacture_id',$menu_id)
            ->update(['publication_status'=>0]);

        return Redirect::to ('admin/manufacture/list');



    }
    public function active($menu_id){
        DB::table('manufacture')
            ->where('manufacture_id',$menu_id)
            ->update(['publication_status'=>1]);
          return Redirect::to('admin/manufacture/list');
    }


    public function edit($id)
    {
        $manu_info=DB::table('manufacture')
            ->where('manufacture_id',$id)
            ->first();

  $manu_info= view('admin.item.edit_manu')
      ->with('manu_info',$manu_info);
      return view('admin.layouts.master')
          ->with('admin.item.edit_manu',$manu_info);


//        return view('admin.item.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

// code ta vallagse but update hoina bcz fileld jekono akta khali theke jai

//        $find=Item::find($id);
//        $catch=($request->all());
//        $Mail=$request->get('manufacture_title');
//
//        $Mail=$request->get('manufacture_details');
//       $find->fill($catch)->save();
//        return Redirect::to('/admin/manufacture/list');

        $data=array();
       $data['manufacture_title']=$request->manufacture_title;
      $data['manufacture_details']=$request->manufacture_details;
       DB::table('manufacture')
           ->where('manufacture_id',$id)
           ->update($data);
        return Redirect::to('/admin/manufacture/list');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('manufacture')
            ->where('manufacture_id',$id)
            ->delete();
        return Redirect::to('/admin/manufacture/list');
    }
}
