<?php

namespace App\Http\Controllers\Admin;

use App\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use DB;
use App\Http\Requests;
use Illuminate\support\Facades\Redirect;
use App\Http\Controllers\Admin\cms;


session_start();


class ProductController extends Controller
{
    public function create(){

        return view('admin.product.pro_create');
    }


    public function store(Request $request){

        /*$data=($request->all());
        Product::create($data);
        return redirect()->back();*/

        /*Product::create([

            'id'=>request('id'),
            'title'=>request('title')
        ]);*/

        $data=array();

        $data['product_name']=$request->product_name;
        $data['category_id']=$request->category_id;
        $data['manufacture_id']=$request->manufacture_id;
        $data['pro_shortdetails']=$request->pro_shortdetails;
        $data['pro_longdetails']=$request->pro_longdetails;
        $data['product_price']=$request->product_price;
        $data['product_size']=$request->product_size;
        $data['product_color']=$request->product_color;
        $data['publication_status']=$request->publication_status;

        $image=$request->file('product_image');
        if($image){
            $image_name=str_random(20);
            $ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'.'.$ext;
            $image_upload_path='image/';
            $image_url=$image_upload_path.$image_full_name;
            $succes=$image->move($image_upload_path,$image_full_name);
            if($succes){
                $data['product_image']=$image_url;
                DB::table('products')
                    ->insert($data);
                Session::put('message','succesfully products added');
                return Redirect::to('admin/add_products');
            }




        }
        $data['product_image']='';
        DB::table('products')->insert($data);
        Session::put('message','without image succesfully products add');
        return Redirect::to('admin/add_products');



    }


    public function show(){

        $table= DB::table('products')
            ->join('categories','products.category_id','=','categories.id')
            ->join('manufacture','products.manufacture_id','=','manufacture.manufacture_id')
            ->select('products.*','categories.title','manufacture.manufacture_title')
            ->get();
//        echo "<pre>";
//        print_r($table);
//        echo "</pre>";
//        exit();

//        $fetch=Product::all();
        $data=[
            'products'=>$table,
        ];

        return view('admin.product.pro_show',$data);

    }
    public function unactive($id){
        DB::table('products')
            ->where('product_id',$id)
            ->update(['publication_status'=>0]);
        Session::put('message',' this product succesfully is pause');
        return Redirect::to ('admin/show_products');

        }

    public function active($id){

        DB::table('products')->where('product_id',$id)->update(['publication_status'=>1]);
        Session::put('message',' product succesfully run');
        return Redirect::to('admin/show_products');
    }



    public function edit($id)
    {
        $product_info=DB::table('products')
            ->where('product_id',$id)
            ->first();

        $product_info= view('admin.product.product_edit')
            ->with('product_info',$product_info);
        return view('admin.layouts.master')
            ->with('admin.product.product_edit',$product_info);



    }
    public function update(Request $request,$product_id){


        $data=array();

            $data['product_name']=$request->product_name;
            $data['category_id']=$request->category_id;
            $data['manufacture_id']=$request->manufacture_id;
            $data['pro_shortdetails']=$request->pro_shortdetails;

            $data['pro_longdetails']=$request->pro_longdetails;


            $data['product_price']=$request->product_price;
            $data['product_size']=$request->product_size;
            $data['product_color']=$request->product_color;




            $image=$request->file('product_image');

            $image=$request->file('product_image');
            if($image){
                $image_name=str_random(20);
                $ext=strtolower($image->getClientOriginalExtension());
                $image_full_name=$image_name.'.'.$ext;
                $image_upload_path='image/';
                $image_url=$image_upload_path.$image_full_name;
                $succes=$image->move($image_upload_path,$image_full_name);

                $data['product_image']=$image_url;


                }

                DB::table('products')
            ->where('product_id',$product_id)
            ->update($data);

        return Redirect::to('admin/show_products');



    }

    public function delete($product_id){

        DB::table('products')
            ->where('product_id',$product_id)
            ->delete();
         return redirect()->back();
    }



}

