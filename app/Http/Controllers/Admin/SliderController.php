<?php

namespace App\Http\Controllers\Admin;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

session_start();

class SliderController extends Controller
{
    public function index(){

    return view('admin.slider.slider_add');
    }

    public function store(Request $request){

        $data=array();
        $data['publication_status']=$request->publication_status;

        $image=$request->file('slider_image');
        if($image){
            $image_name=str_random(20);
            $ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'.'.$ext;
            $image_upload_path='slider/';
            $image_url=$image_upload_path.$image_full_name;
            $succes=$image->move($image_upload_path,$image_full_name);
            if($succes){
                $data['slider_image']=$image_url;
                DB::table('sliders')
                    ->insert($data);
                Session::put('message','succesfully slider added');
                return Redirect::to('admin/add_slider');

            }


            }

    }

    public function show(){

//        $data=DB::table('sliders')
//            ->where('publication_status',1)
//            ->get();
        $sliders=Slider::all();

        $table=[
            'sliders'=>$sliders,
        ];
        return view('admin.slider.slider_show',$table);

    }

    public function unactive($slider_id){
        DB::table('sliders')
            ->where('slider_id',$slider_id)
            ->update(['publication_status'=>0]);
        return Redirect::to('admin/slider_show');
    }
    public function active($slider_id){

        DB::table('sliders')
            ->where('slider_id',$slider_id)
            ->update(['publication_status'=>1]);
        return Redirect::to("admin/slider_show");
    }

    public function delete($slider_id){
        DB::table('sliders')
            ->where('slider_id',$slider_id)
            ->delete();
        return redirect()->back();
    }


}
