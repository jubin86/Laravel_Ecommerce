<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable=['product_name','category_id','manufacture_id','pro_shortdetails','pro_longdetails',

        'product_price','product_size','product_color','product_image','publication_status'];
}
