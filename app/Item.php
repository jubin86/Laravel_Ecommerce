<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $table="manufacture";
    protected $fillable=['manufacture_title','manufacture_details','publication_status'];

//    public function category(){
//        return $this->belongsTo(Category::class);
//    }
}
