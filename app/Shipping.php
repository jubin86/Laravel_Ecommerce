<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected  $fillable=['shipping_mail','firstname','lastname','address','city','mobile_number'];


}
