<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', function () {

return view('home');
});


Route::get('/test','HomeController@test');

Route::get('/admin/users','UserController@index');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard','Admin\AdminController@getDashbroad')->middleware('auth');


Route::get('/admin/category/add', 'Admin\CategoryController@create')->middleware('auth');
Route::post('/admin/category/store','Admin\CategoryController@store')->middleware('auth');
Route::get('/admin/category/list','Admin\CategoryList@show')->middleware('auth');
Route::get('/admin/unactive-category/{id}','Admin\CategoryList@unactive');
Route::get('/admin/active-category/{id}','Admin\CategoryList@active');
Route::get('/admin/edit-category/{id}','Admin\CategoryList@edit');
Route::post('/admin/update-category/store/{id}','Admin\CategoryList@update')->name('admin.update-category.store');
Route::get('/admin/delete-category/{id}','Admin\CategoryList@delete');


//manufacture part

Route::get('/admin/manufacture/add','Admin\ManufactureController@create')->middleware('auth');
Route::post('/admin/manufacture/store','Admin\ManufactureController@store')->middleware('auth');
Route::get('/admin/manufacture/list','Admin\ManufactureController@show')->middleware('auth');
Route::get('/admin/manufacture_pause/{manufacture_id}','Admin\ManufactureController@unactive');
Route::get('admin/manufacture_run/{manufacture_id}','Admin\ManufactureController@active');
Route::get('admin/manufacture_edit/{manufacture_id}','Admin\ManufactureController@edit');
Route::post('admin/update_manu/store/{manufacture_id}','Admin\ManufactureController@update')->name('admin.update_manu.store');
Route::get('admin/manufacture_delete/{manufacture_id}','Admin\ManufactureController@destroy');


//products.........//

Route::get('admin/add_products','Admin\ProductController@create');

Route::post('admin/product_store','Admin\ProductController@store');
Route::get('admin/show_products','Admin\ProductController@show');
Route::get('/admin/pause-product/{id}','Admin\ProductController@unactive');
Route::get('/admin/active-product/{id}','Admin\ProductController@active');
Route::get('admin/product_edit/{id}','Admin\ProductController@edit');
Route::post('admin/update_product/store{product_id}','Admin\ProductController@update')->name('admin.update_product.store');
Route::get('admin/product_delete/{id}','Admin\ProductController@delete');


//Slider image

Route::get('admin/add_slider','Admin\SliderController@index');
Route::post('admin/slider_store','Admin\SliderController@store');
Route::get('admin/slider_show','Admin\SliderController@show');
Route::get('admin/slider-unactive/{slider_id}','Admin\SliderController@unactive');
Route::get('admin/slider-active/{slider_id}','Admin\SliderController@active');
Route::get('admin/slider-delete/{slider_id}','Admin\SliderController@delete');

//Manage product............

Route::get('admin/manage_product','Admin\ManageController@index');
Route::get('admin/manage_order_show/{order_id}','Admin\ManageController@create');




//.........forntend

Route::get('/','User\UserController@index');
Route::get('/home','User\UserController@home');

//product wise category
Route::get('/product_by_category/{id}','User\ProcatshowController@index');
Route::get('/product_by_manufacture/{manufacture_id}','User\ProcatshowController@pro_by_menu');

Route::get('/product_details/{product_id}','User\ProcatshowController@product_view');


//cart controler is here..........
Route::post('/add_cart/','User\CartController@add_cart');
Route::get('/add_cart_store','User\CartController@add_cart_value_store');
Route::get('/delete_cart/{rowId}','User\CartController@delete_cart');
Route::post('/update_cart/','User\CartController@update_cart');

Route::get('/customer_login','User\CheckoutController@customer_login');
Route::post('/customer_signup','User\CheckoutController@store');
Route::get('/customer_shipping','User\CheckoutController@customer_shipping');

Route::post('/shipping_store','User\CheckoutController@shipping_save');
Route::get('/payment_option','User\CheckoutController@payment_option');

Route::post('/payment_gateway','User\CheckoutController@payment_gateway');


Route::post('/login_check','User\CheckoutController@login_check');
Route::get('/customer_logout','User\CheckoutController@customer_logout');











